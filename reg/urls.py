from django.urls import path
from .views import index

app_name = 'reg'

urlpatterns = [
    path('', index, name='index'),
]