from django import forms
from .models import Person
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget

class PersonForm(forms.ModelForm):
	full_name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
	phone_number = PhoneNumberField(
		widget = PhoneNumberPrefixWidget(initial = "UZ", attrs={"class":"form-control"}),

	)

	class Meta:
		model = Person
		fields = "__all__"
		