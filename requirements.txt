Django==3.2.4
asgiref==3.3.4
phonenumbers==8.12.25
django-phonenumber-field==5.2.0
Babel
pytz==2021.1
sqlparse==0.4.1
